import React from 'react';
import AppRouter from './src/navigation/AppRouter';


const App = () => {
  return (
      <AppRouter/>
  );
};

export default App;
