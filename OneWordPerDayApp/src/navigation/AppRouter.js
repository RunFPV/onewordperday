import React from 'react';
import SplashScreen from '../screens/SplashScreen';
import {
    getRandomWord,
    getDefinitionWord,
    getQuotesWord,
    getScrabbleScoreWord,
    getAntonymesWord,
    getSynonymeWord,
    getExpressionsWord,
} from '../api/api';
import AppNavigator from './AppNavigator';


class AppRouter extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            isLoading: true,
            loadInfo: 'Chargement du mot...',
            wordRes: null,
        };
    }

    fakeData = () => {
        this.setState({
            wordRes: {
                antoWord:
                    [
                        {
                            dicolinkUrl: 'https://www.dicolink.com/mots/adjudicataire',
                            mot: 'adjudicataire',
                        },
                        {
                            dicolinkUrl: 'https://www.dicolink.com/mots/encherisseuse',
                            mot: 'encherisseuse',
                        },

                    ],
                attributionText: 'Dictionnaire Larousse de la langue française numérisé',
                attributionUrl: 'https://www.larousse.fr/',
                auteur: 'Tristan Bernard',
                citation: 'Notaire : arrive souvent au dernier acte.',
                definition: 'Officier public et ministériel chargé de rédiger ou de recevoir les actes et contrats auxquels les personnes doivent ou veulent faire donner un caractère d\'authenticité, et particulièrement tous actes relatifs à la vente d\'un immeuble, au règlement d\'une succession.',
                dicolinkUrl: 'https://www.dicolink.com/mots/notaire',
                id: 'M49445-122381',
                mot: 'notaire',
                nature: 'nom masculin',
                score: 7,
                source: 'larousse',
                synoWord:
                    [
                        {
                            dicolinkUrl: 'https://www.dicolink.com/mots/clerc',
                            mot: 'clerc',
                        },
                        {
                            dicolinkUrl: 'https://www.dicolink.com/mots/huissier',
                            mot: 'huissier',
                        },
                        {
                            dicolinkUrl: 'https://www.dicolink.com/mots/adjudicateur',
                            mot: 'adjudicateur',
                        },
                        {
                            dicolinkUrl: 'https://www.dicolink.com/mots/sédentaire',
                            mot: 'sédentaire',
                        },
                        {
                            dicolinkUrl: 'https://www.dicolink.com/mots/tabellion',
                            mot: 'tabellion',
                        },
                    ],
            },
            isLoading: false,
            loadInfo: '',
        });
    };


    getData = async () => {
        const wordRes = await getRandomWord();
        const word = wordRes[0].mot;

        this.setState({loadInfo: 'Récupération de la définition...'});
        const defRes = await getDefinitionWord(word);
        this.setState({loadInfo: 'Récupération de la citation...'});
        const quotesWord = await getQuotesWord(word);
        this.setState({loadInfo: 'Récupération du score au Scrabble...'});
        const scrabbleScoreWord = await getScrabbleScoreWord(word);
        this.setState({loadInfo: 'Récupération des synonymes...'});
        const synoWord = await getSynonymeWord(word);
        this.setState({loadInfo: 'Récupération des antonymes...'});
        const antoWord = await getAntonymesWord(word);
        this.setState({loadInfo: 'Récupération des expressions...'});
        const exprWord = await getExpressionsWord(word);

        this.setState({
            wordRes: {...defRes[0], ...quotesWord[0], ...scrabbleScoreWord, antoWord, synoWord, exprWord},
            isLoading: false,
            loadInfo: '',
        });
    };

    async componentDidMount() {
        this.fakeData();
        //this.getData();
    }


    render() {
        if (this.state.isLoading) {
            return (
                <SplashScreen loadInfo={this.state.loadInfo}/>
            );
        }
        return (
            <AppNavigator/>
        );
    }
}


export default AppRouter;
