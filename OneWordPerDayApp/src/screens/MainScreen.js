import React from 'react';
import {SafeAreaView, Text, StyleSheet, StatusBar} from 'react-native';

class MainScreen extends React.Component {
    render() {
        return (
            <SafeAreaView style={styles.container}>
                <StatusBar barStyle="light-content"/>
                <Text style={styles.title}>La vue principale</Text>
            </SafeAreaView>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: '#34495e',
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    title: {
        color: 'white',
        fontSize: 30,
        fontWeight: 'bold',
    },
});


export default MainScreen;
