import React from 'react';
import {View, StyleSheet, Text} from 'react-native';


class Quote extends React.Component{
    constructor(props) {
        super(props);
        console.log(this.props);
    }

    render() {
        return(
            <View style={style.content}>
                <Text style={style.title}>Citation : </Text>
                <View style={style.contentValue}>
                    <Text style={style.value}>{this.props.quote ? "\"" +this.props.quote + "\"" +" - "+ this.props.author : "Aucune donnée trouvée"}</Text>
                </View>
            </View>
        );
    }
}


const style = StyleSheet.create({
    content: {
        // flex: 1,
        margin: 10,
        alignItems: 'stretch',
        justifyContent: 'center',
    },
    title: {
        fontSize: 20,
        color: 'white',
        textDecorationLine: 'underline',
    },
    contentValue: {
        flexDirection: 'row',
        flexWrap: 'wrap',
        paddingHorizontal: 15,
    },
    value: {
        color: 'white',
        fontSize: 18,
    }
});


export default Quote;
