import React from 'react';
import {View, StyleSheet, Text} from 'react-native';


class Footer extends React.Component {
    render() {
        return(
            <View style={style.footer}>
                <Text style={style.source}>Source : {this.props.word.attributionText} - {this.props.word.attributionUrl}</Text>
            </View>
        );
    }
}

const style = StyleSheet.create({
    footer : {
        // flex: 0,
        margin: 10,
    },
    source : {
        color: 'white',
    }
});

export default Footer;
